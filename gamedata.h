#pragma once
#include <QVariant>
#include <QObject>
#include <QList>
#include "monster.hpp"
#include "trainer.h"
#include "skill.hpp"
#include "assets/cpp/qqmlhelpers.h"


class GameData : public QObject{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QList<Skill *>, dataSkill)
    QML_WRITABLE_PROPERTY(QList<QObject *>, dataMonster)
    QML_WRITABLE_PROPERTY(Trainer *, player)
public:
    GameData();

    void newSkill(QString name, int power, int accuracy, Type type);
    void newMonster(QString name,int attack,int mAttack, int defense,int mDefense,int speed,int hp);
    void newMonster(QObject *monster);
    void loadSkill(QString path = "/home/matheus/build-BattleMonster/data/json/skill.json");
    void saveSkill(QString path = "/home/matheus/build-BattleMonster/data/json/skill.json");
    void loadMonster(QString path = "/home/matheus/build-BattleMonster/data/json/monster.json");
    void saveMonster(QString path = "/home/matheus/build-BattleMonster/data/json/monster.json");
    void loadTrainer(QString path = "/home/matheus/build-BattleMonster/data/player/player.json");
    void saveTrainer(QString path = "/home/matheus/build-BattleMonster/data/player/player.json");
    Q_INVOKABLE void newTrainer(QString name,QString sex,int monsterId);
    Q_INVOKABLE void addMonster(int id,int level = 5);
    QObject *atMonster(int id) const;
};
