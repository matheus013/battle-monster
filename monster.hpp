#pragma once
#include <QObject>
#include <QList>
#include "skill.hpp"
#include "assets/cpp/qqmlhelpers.h"

class Monster : public QObject{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QString, name)
    QML_WRITABLE_PROPERTY(int, attack)
    QML_WRITABLE_PROPERTY(int, mAttack)
    QML_WRITABLE_PROPERTY(int, defense)
    QML_WRITABLE_PROPERTY(int, mDefense)
    QML_WRITABLE_PROPERTY(int, speed)
    QML_WRITABLE_PROPERTY(double, hp)
    QML_WRITABLE_PROPERTY(int, level)
    QML_WRITABLE_PROPERTY(QList<QObject*>, skill)

public:
    Monster();
    Monster(QString name,int attack,int mAttack, int defense,int mDefense,int speed,int hp);
    Monster(QJsonObject json);
    void levelUp();
    QJsonObject toJson();
};
