#ifndef TRAINER_H
#define TRAINER_H
#include "monster.hpp"
#include <QObject>

class Trainer : public QObject{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QString, name)
    QML_WRITABLE_PROPERTY(QString, sex)
    QML_WRITABLE_PROPERTY(QList<QObject*>, team)

public:
    Trainer(QJsonObject json);
    Trainer(QString name,QString sex,int initial);
    void addMonster(Monster *curr);
};

#endif // TRAINER_H
