#ifndef ENEMYBUILDER_H
#define ENEMYBUILDER_H
#include "abstractbuilder.h"

class EnemyBuilder : public AbstractMonsterBuilder{
    Q_OBJECT
public:
    void buildAttack();
    void buildDefense();
    void buildMAttack();
    void buildHp();
    void buildSpeed();
    void buildMDefense();
    void buildName(QString name);

};

#endif // ENEMYBUILDER_H
