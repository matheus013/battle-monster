#ifndef BATTLE_H
#define BATTLE_H
#include <QObject>
#include "trainer.h"
#include "assets/cpp/qqmlhelpers.h"
class Battle : public QObject{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(Trainer *, player)
    QML_WRITABLE_PROPERTY(Monster *, enemy)
    QML_WRITABLE_PROPERTY(Monster *, front)
    QML_WRITABLE_PROPERTY(int, playerHp)
    QML_WRITABLE_PROPERTY(int, enemyHp)

public:
    Battle();
    Q_INVOKABLE void nextEnemy(int level = 5);
    Q_INVOKABLE void nextFront();
    Q_INVOKABLE void reloadPlayer();

};

#endif // BATTLE_H
