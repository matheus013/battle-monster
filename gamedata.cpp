#include "gamedata.h"
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
GameData::GameData(){}

void GameData::newSkill(QString name, int power, int accuracy, Type type){
    Skill *skill = new Skill();
    skill->setProperty("name",name);
    skill->setProperty("type",type);
    skill->setProperty("power",power);
    skill->setProperty("accuracy",accuracy);
    m_dataSkill.append(skill);
}

void GameData::newMonster(QString name, int attack, int mAttack, int defense, int mDefense, int speed, int hp){
    Monster *monster = new Monster(name,attack,mAttack,defense,mDefense,speed,hp);
    m_dataMonster.append(monster);
}

void GameData::newTrainer(QString name, QString sex, int monsterId){
    m_player = new Trainer(name,sex,monsterId);
    saveTrainer();
}

void GameData::loadSkill(QString path){
    QFile in(path);
    QJsonDocument doc;
    if(in.open(QIODevice::ReadOnly)){
        doc = QJsonDocument::fromJson(in.readAll());
        QJsonArray json = doc.array();
        for (int var = 0; var < json.size(); ++var){
            Skill *aux = new Skill(json.at(var).toObject());
            m_dataSkill.append(aux);
        }
    }
}

void GameData::loadMonster(QString path){
    QFile read(path);
    QJsonDocument doc;
    if(read.open(QIODevice::ReadOnly)){
        doc = QJsonDocument::fromJson(read.readAll());
        QJsonArray json = doc.array();
        //        qDebug() << json;
        for (int var = 0; var < json.size(); ++var) {
            Monster* monster = new Monster(QJsonObject(json.at(var).toObject()));
            m_dataMonster.append(monster);
        }
    }
}

void GameData::saveMonster(QString path){
    QJsonArray json;
    for (int var = 0; var < m_dataMonster.size(); ++var) {
        Monster* monster = (Monster*) m_dataMonster.at(var);
        json.append(monster->toJson());
    }
    QJsonDocument dataDoc(json);
    //    qDebug() << path;
    QFile out(path);
    if(out.open(QIODevice::WriteOnly | QIODevice::Text)){
        out.write(dataDoc.toJson());
    }else{
        qDebug() << Q_FUNC_INFO << "error";
    }
}

void GameData::loadTrainer(QString path){
    QFile read(path);
    QJsonDocument doc;
    if(read.open(QIODevice::ReadOnly)){
        doc = QJsonDocument::fromJson(read.readAll());
        QJsonObject json = doc.object();
        m_player = new Trainer(json);
    }
    qDebug() << "Team size: " << m_player->get_team().size();
}

void GameData::saveTrainer(QString path){
    QFile out(path);
    QJsonObject json;
    QJsonArray list;
    json.insert("name",m_player->get_name());
    json.insert("sex",m_player->get_sex());
    for (int var = 0; var < m_player->get_team().size(); ++var) {
        QJsonObject aux;
        Monster *monster= (Monster *)m_player->get_team().at(var);
        aux.insert("id",monster->property("name").toInt());
        aux.insert("level",monster->get_level());
        list.append(aux);
    }
    json.insert("monsterList",list);
    QJsonDocument doc(json);
    if(out.open(QIODevice::WriteOnly)){
        out.write(doc.toJson());
    }
}

QObject *GameData::atMonster(int id) const{
    //    qDebug() << Q_FUNC_INFO << id;
    return get_dataMonster().at(id);
}

void GameData::addMonster(int id, int level){
    if(id >= get_dataMonster().size())
        return;
    Monster * curr = (Monster*)atMonster(id);
    curr->set_level(level);
    get_player()->addMonster(curr);
    saveTrainer();
}

void GameData::newMonster(QObject *monster){
    m_dataMonster.append(monster);
}
