#include "trainer.h"
#include <QJsonObject>
#include <QJsonArray>
#include "gamedata.h"
#include <QDebug>

Trainer::Trainer(QJsonObject json){
    GameData data;
    data.loadMonster();
    m_name =  json.value("name").toString();
    m_sex = json.value("sex").toString();
    QJsonArray aux = json.value("monsterList").toArray();
    for (int var = 0; var < aux.size(); ++var) {
        Monster * monster = (Monster*)data.atMonster(aux.at(var).toObject().value("id").toInt());
        monster->set_level(aux.at(var).toObject().value("level").toInt());
        m_team.append(monster);
    }
}

Trainer::Trainer(QString name, QString sex, int initial){
    m_name = name;
    m_sex = sex;
    GameData data;
    data.loadMonster();
    m_team.append(data.atMonster(initial));

    qDebug() << Q_FUNC_INFO << data.atMonster(initial)->property("name").toInt();
}
void Trainer::addMonster(Monster *curr){
        m_team.append(curr);
}

