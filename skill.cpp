#include "skill.hpp"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QVariant>

Skill::Skill() {}

Skill::Skill(QJsonObject json){
    m_name = json.value("name").toString();
    m_accuracy = json.value("accuracy").toInt();
    m_type = static_cast<Type>(json.value("type").toInt());
    m_power = json.value("power").toInt();
}

QJsonObject Skill::toJson(){
    QJsonObject json;
    json.insert("name",QVariant(m_name).toString());
    json.insert("power",QVariant(m_power).toInt());
    json.insert("accuracy",QVariant(m_accuracy).toInt());
    json.insert("type",m_type);
    //    if(json.value("type") == m_type)
    //        qDebug() << Q_FUNC_INFO << "enum: ok";
    return json;
}
