#pragma once
#include <QObject>
#include "enum.h"
#include "assets/cpp/qqmlhelpers.h"

class Skill : public QObject {
    Q_OBJECT
    Q_ENUMS(Type)
    QML_WRITABLE_PROPERTY(int, power)
    QML_WRITABLE_PROPERTY(QString, name)
    QML_WRITABLE_PROPERTY(Type, type)
    QML_WRITABLE_PROPERTY(int, accuracy)
public:
    Skill();
    Skill(QJsonObject json);
    QJsonObject toJson();
};
