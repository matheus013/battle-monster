#ifndef BUILDER_H
#define BUILDER_H
#include <QObject>
#include "enemybuilder.h"


class Builder : public QObject{
    Q_OBJECT
    EnemyBuilder monsterBuilder;
public:
    Builder();
    void buildMonster(QString name);
    Monster* getMonster();

};

#endif // BUILDER_H
