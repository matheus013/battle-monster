#include "enemybuilder.h"
#define RANDOM qrand() % 100 + 300
void EnemyBuilder::buildAttack(){
    object->set_attack(RANDOM);
}

void EnemyBuilder::buildDefense(){
object->set_defense(RANDOM);
}

void EnemyBuilder::buildMAttack(){
object->set_mAttack(RANDOM);
}

void EnemyBuilder::buildHp(){
    object->set_hp(RANDOM);
}

void EnemyBuilder::buildSpeed(){
    object->set_speed(RANDOM);
}

void EnemyBuilder::buildMDefense(){
    object->set_mDefense(RANDOM);
}

void EnemyBuilder::buildName(QString name){
    object->set_name(name);
}
