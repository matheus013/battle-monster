#include "monster.hpp"
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>

Monster::Monster(){
    m_level = 5;
}

Monster::Monster(QString name, int attack, int mAttack, int defense, int mDefense, int speed, int hp){
    m_attack = attack;
    m_name = name;
    m_defense = defense;
    m_mAttack = mAttack;
    m_mDefense = mDefense;
    m_speed = speed;
    m_hp = hp;
    m_level = 1;
}

Monster::Monster(QJsonObject json){
    m_name = json.value("name").toString();
    m_attack = json.value("attack").toInt();
    m_mAttack = json.value("mAttack").toInt();
    m_defense = json.value("defense").toInt();
    m_mDefense = json.value("mDefense").toInt();
    m_speed = json.value("speed").toInt();
    m_hp = json.value("hp").toInt();
}

void Monster::levelUp(){
    m_level++;
}

QJsonObject Monster::toJson(){
    QJsonObject json;
    json.insert("name",m_name);
    json.insert("attack",m_attack);
    json.insert("mAttack",m_mAttack);
    json.insert("defense",m_defense);
    json.insert("mDefense",m_mDefense);
    json.insert("speed",m_speed);
    json.insert("hp",m_hp);
    //    if(json.value("type") == m_type)
    //        qDebug() << Q_FUNC_INFO << "enum: ok";
    return json;

}
