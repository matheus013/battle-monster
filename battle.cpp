#include "battle.h"
#include "gamedata.h"
#include <QDebug>
#include <QJsonObject>
Battle::Battle(){}

void Battle::nextEnemy(int level){
    GameData data;
    data.loadMonster();
    int id = qrand() % (98 + 1);
    //qDebug() << Q_FUNC_INFO << id;
    set_enemy((Monster*)data.atMonster(id));
    m_enemy->set_level(level);
    set_enemyHp(m_enemy->get_hp());
}

void Battle::nextFront(){
    Monster *monster = (Monster *)get_player()->get_team().front();
    set_front(monster);
    set_playerHp(get_front()->get_hp());
}

void Battle::reloadPlayer(){
    GameData data;
    data.loadTrainer();
    set_player(data.get_player());
    nextFront();
}
