#ifndef ABSTRACTBUILDER_H
#define ABSTRACTBUILDER_H
#include <QObject>
#include "monster.hpp"
class AbstractMonsterBuilder : public QObject{
    Q_OBJECT
protected:
    Monster *object;
public:
    AbstractMonsterBuilder(){
        object = new Monster();
    }
    virtual void buildName(QString name) = 0;
    virtual void buildAttack() = 0;
    virtual void buildMAttack() = 0;
    virtual void buildDefense() = 0;
    virtual void buildMDefense() = 0;
    virtual void buildSpeed() = 0;
    virtual void buildHp() = 0;
    Monster* get(){
        return object;
    }
};

#endif // ABSTRACTBUILDER_H
