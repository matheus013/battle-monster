#include "datacreate.h"
#include <QDebug>
#include "builder.h"

DataCreate::DataCreate() {}

void DataCreate::buildMonster(){
    Builder build;
    data.loadMonster();
    data.get_dataMonster().clear();
    for (int var = 0; var < 99; ++var) {
        build.buildMonster(QString().setNum(var));
        qDebug() << build.getMonster()->get_attack();
        data.newMonster(build.getMonster());
    }
    data.saveMonster();
}

int DataCreate::random(int a, int b){
    return qrand() % ((b + 1) - a) + a;
}

