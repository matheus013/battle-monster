#include "builder.h"

Builder::Builder(){

}

void Builder::buildMonster(QString name){
    monsterBuilder.buildAttack();
    monsterBuilder.buildDefense();
    monsterBuilder.buildHp();
    monsterBuilder.buildMAttack();
    monsterBuilder.buildMDefense();
    monsterBuilder.buildSpeed();
    monsterBuilder.buildName(name);
}

Monster *Builder::getMonster(){
    return monsterBuilder.get();
}

